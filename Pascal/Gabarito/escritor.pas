Unit Escritor;

interface
    procedure escreve (c:Char; var out:TextFile);

implementation

Uses Helper;

procedure escreve (c:Char; var out:TextFile);
  begin
    prepare_file(out);
    write(out, c);
  end;
end.
