#include <stdio.h>
#include <stdlib.h>

long long fatorial(long long n)
{
	long long fatorial = 1;
	for(int i = 2; i < n; i++)
		fatorial *= i;
	return fatorial * n;
}

int main(int argc, char **argv, char **envp)
{
	if(argc > 1)
	{
		printf("%lld\n", fatorial(atoll(argv[1])));
	}
	else
	{
		puts("Voce precisa dar um valor para n!");
	}
}