program Fatorial;

uses sysutils;

function fatorial(n: int64): int64;
var i : Longint;
begin
  fatorial := 1;
  for i := 2 to n do
    fatorial := fatorial * i;  
end;

begin
  if (ParamCount > 0) then
    writeln(fatorial(strToInt(ParamStr(1))))
  else
    writeln('Voce precisa dar um valor para n!');
end.
