     In the mind as in a field, some things may be sown and carefully
     brought up, yet that which springs naturally is most pleasing.
                                                              --TACITUS.


The year 1805 saw Beethoven hard at work in a field new to
him,--operatic composition. It had probably been in his mind for some
years to write an opera. In those days almost every composer wrote
operas, and to have written a successful one carried with it, not only a
certain prestige, but substantial rewards in a financial sense. Outside
of the church but little opportunity was afforded the general public to
gratify its love for music other than in opera. Orchestral concerts were
comparatively rare,--song recitals unknown. The development of the
orchestra was just beginning, through the genius of Beethoven, and the
Viennese were to a great extent, still unconscious of its importance, as
a means of musical expression. The many symphonies, quartets, and other
forms of chamber-music of Haydn, Mozart and contemporaneous composers,
were for the most part written for private performance at musical
functions in the houses of the nobility, or for friends of the
composers.

Beethoven believed that if he were to write one or two operas, his
income would be reinforced to such an extent as to enable him to give
his attention wholly to the production of symphonies and masses, a style
of composition to which he was inclined by temperament. In the early
symphonies we already have a foreshadowing of what he could do in the
production of great orchestral music, the desire for which in later
years controlled him wholly. Like most men of genius Beethoven had
little regard for money, and until middle age was reached, never thought
of saving any. He valued it only in so far as he could use it for
himself or others. It may be said in passing that he gave it away
freely, glad to be of service to others. His income, augmented by his
copyrights, did not keep pace with his expenditures; when a friend
needed money and he had none, he would give him a composition instead,
which the other would turn into cash.

The manager of the theatre, An der Wien, had, before this, made
overtures to Beethoven to write an opera, and he went so far as to take
up his quarters in the theatre, preparatory to this work; but a change
in the management made it necessary to give up the idea for the time
being. In 1804, the offer in regard to the opera was renewed, and work
was begun upon it. It took up a large part of his time until its
production in November of 1805. It is probable that he took more pains
with this work than was devoted to any other of his compositions with
the exception of the Mass in D. His capacity for work was extraordinary,
particularly at this time, and the delight that he experienced in
producing these masterpieces was still new to him, which in itself was
an incentive to great exertion. His approaching deafness also had a good
deal to do with his great activity. The ailment had progressed steadily
from the time of its first appearance; at the time of which we write he
had abandoned all hope of any aid from medical treatment; by throwing
himself heart and soul into his work, he could forget for the time the
misfortune which was closing in on him. He feared that a period of
absolute deafness might set in when he would be unable to hear any of
his works, and the desire must have been great to accomplish as much as
possible before that time should come.

Beethoven does not seem to have been very hard to suit in the way of a
libretto at this time. He probably gave the matter very little
consideration except on one point,--its morality. His high ideals, and
his innate purity of mind, caused him to dislike and condemn the sort of
story which was usually worked up into operatic libretti in those days,
in which intrigue and illicit love formed the staple material. He
expressed himself strongly on this subject, even criticising Mozart for
having set Don Giovanni to music, saying that it degraded the art. So
strongly did he feel about it that he seems to have thought almost any
libretto would do, provided the moral sentiment contained in it were
sufficiently prominent. Later, the experience which he gained with
Fidelio showed him that the libretto of an opera is indeed a very
important matter; then he went to the other extreme, and was unable to
find anything which would satisfy him, although many libretti were
submitted to him at various times during the remainder of his life. A
quantity of them were found among his papers after his death. Bouilly's
libretto Leonore, which had been set to music by two different composers
before Beethoven took it in hand, was finally selected, and Sonnleithner
was employed to translate it from the French. The name of the opera was
changed to Fidelio, but the various overtures written for it are still
known as the Leonore overtures.

Beethoven took up his quarters in the theatre again as soon as the
libretto was ready for him and went to work at it with a will. But he
was not at his best in operatic writing,--this symphonist, this creator
of great orchestral forms. The opera was an alien soil to him;
composition--never an easy matter to Beethoven, was more difficult than
ever in the case of Fidelio. The sketch-books show the many attempts and
alterations in the work, at its every stage. In addition, he was
handicapped at the outset by an unsuitable libretto. The Spanish
background, for one thing, was a clog, as his trend of thought and
sympathies were thoroughly German. But this is a slight matter compared
with the forbidding nature of the drama itself, with its prison scenes,
its dungeons and general atmosphere of gloom. One dreary scene after
another is unfolded, and the action never reaches the dignity of tragedy
nor the depth of pathos which should be awakened by the portrayal of
suffering. We are unable to feel that the two principal characters are
martyrs; as one tiresome scene succeeds another, we come to care nothing
whatever about them and are unable to sympathize with them in their
suffering or rejoice in their deliverance. The first requisite in opera,
it would appear, is that it be pervaded by an atmosphere of
romanticism. Other things are necessary; the libretto must have dramatic
situations; but above all, the romantic element must prevail. If it is
difficult for the listener to become interested in an opera with such a
libretto as is Fidelio, it must be doubly so for the composer who
undertakes the task of writing music for it. A dull story hinders the
play of fancy; the imagination remains dormant, and the product under
such conditions has the air of being forced. The musician is in bonds.

Musically, it is a work of surpassing beauty; but there is a dissonance
between music and libretto which gives the impression of something
lacking; there is not the harmony which we expect in a work of this
kind. Wagner has taught us better on these points. The music of Fidelio
has force and grandeur; some of it has a sensuous beauty that reminds us
of Mozart at his best. Had Beethoven's choice fallen to a better
libretto, the result might have been an altogether better opera.

Fidelio affords a good instance of the fact that operatic composition,
considered strictly as music, is not the highest form in which the art
can be portrayed, and that, in itself, it is not so strictly confined to
the domain of music as is the symphony, or the various forms of sacred
music (the oratorio or the mass, for instance). It may, in the right
hands, come to be a greater work of art, viewed in its entirety, than
either of the forms just mentioned. In the hands of a man like Wagner,
it undoubtedly is, but in such a case the result is achieved by means
other than those obtained through the domain of music. Much is
contributed by the literary quality of the libretto, its poetic and
romantic qualities, its dramatic possibilities, as well as its stage
setting and the ability of the singers to act well their parts. An opera
is a combination of several arts, in which music is often subordinated.
Not so in the case of sacred music, in which the entire portrayal rests
absolutely on the musician's art. Of the works of the great composers
who wrote both classes of music, those which are devoted to religious
subjects will be found vastly superior in almost every instance, with
the one exception of Mozart's and in the case of this composer, his Mass
in B flat and the Requiem will bear comparison with any of his operas.
With no regular income, Mozart was compelled to write operas in order to
live, but his preference was for sacred music. Haydn, on the other hand,
spent no time on grand opera. Through his connection with the Princes
Esterhazy, which gave him an assured income from his twenty-ninth year
to the end of his life, he was in a position to write only the style of
music to which he was best adapted by his talents and preference.

Above all other considerations, the opera must be made to pay. The
composers expected to make money from it, and its presentation was
always accompanied by enormous expense. Everything conspired to get them
to write what their audience would like, without considering too closely
whether this was the best they were capable of producing. In those times
all that people required of an opera was that it should entertain. If we
compare the best opera before Wagner's time with such works as Bach's
Grand Mass in B minor, or Beethoven's Mass in D, we will readily see
that the composers of those times put their best thought into their
sacred compositions. Bach, Protestant that he was, but with the vein of
religious mysticism strong in him, which is usually to be found in
highly endowed artistic natures (Wagner is an instance, also Liszt), was
attracted by the beautiful text of the Mass, its stateliness and
solemnity, and the world is enriched by an imperishable work of genius.
It is significant that he wrote no opera, and Beethoven only one. Both
composers probably regarded the opera as being less important
artistically than the other great forms in which music is embodied.

In operatic composition, as we have seen, the musicians of those times
were too apt to write down to their public. No such temptation came to
them in their religious works, as no income was expected from this
source. Here the composer could be independent of his public, so this
branch of the art was developed to a much greater degree than the other.
A high standard was thus reached and maintained in religious music.

Beethoven by temperament was not adapted to operatic composition. He was
too much the philosopher, his aims being higher than were desired by an
operatic audience of that time. He could best express himself in
orchestral music, and his genius drew him irresistibly in this
direction. This predilection appears throughout his works. In his purely
orchestral compositions, his genius has absolute freedom. When he came
to opera he found himself constantly hampered by new and untried
conditions. He soon found that opera has to do with something besides
music. Having once begun, however, he carried it through, perforce, by
almost superhuman efforts.

Wagner, poet that he was, builded better. He had the temperament for
opera. He was adapted to operatic composition as if he had been
specially created for the purpose. Here was the union of the poet and
the musician in the same individual. Knowing the importance of the
drama, and aided by his literary instinct, he was able to select
interesting subjects which were well adapted to musical treatment. It
was the spirit of romanticism pervading these dramas of Wagner's which
enabled him to weave such music about them. We cannot imagine him making
good music to a poor libretto,--with Wagner the libretto and the music
were of equal importance, the two usually having been produced
simultaneously; his music fits the words so well that no other would be
desired.

Early in the summer, Beethoven left his quarters in the theatre and went
into the country nearby, where he could work with more freedom than in
the city. No labor seems to have been too great for him in the
composition of this work. The opera was finished early in the fall of
1805, and as soon as he returned to town he began with the rehearsals.
Then he had almost as much work as in writing the opera, everything
possible having been done to worry him. His simplicity and want of tact
seem to have been very much in evidence at this time; he was like a
child compared with the astute men of affairs with whom he now came in
contact. His greatest difficulty, however, was with his singers. A man
following so faithfully the intimations of his genius as did Beethoven,
withal a man of such striking individuality and force of character,
would be sure to disregard to some extent the capacity of his
performers. His singers made no end of trouble, stating that their parts
were unsingable and asking for alterations. Some of the members of the
orchestra also complained about technical difficulties, but the master
was obdurate, refusing to make any changes. Instead of placating them,
by which means only, a good performance was possible as things went at
that time, he overrode their wishes and would make no concessions
whether in large or in small matters. To Beethoven, music as an art was
the most serious fact in his existence; to the others, it was no more
than a means of enjoyment or of subsistence. His point of view being so
different from that of the others, it is not surprising that he was
always at odds with them. Trifles often annoyed him more than gross
derelictions. At one of the rehearsals the third bassoon player was
absent and Beethoven was enraged. That anything short of illness or
disaster should keep this man from his post was a piece of insolence, an
insult to the art. Prince Lobkowitz was present, and in the effort to
pacify him, made light of the affair; he told him that this man's
absence did not matter much, as the first and second bassoonists were
present, a line of argument that served to include the Prince in
Beethoven's wrath. Hofsekretär Mahler relates the dénouement of the
incident. On the way home, after the rehearsal, as he and Beethoven came
in sight of the Lobkowitz Platz, Beethoven, with the delinquent third
bassoonist still in his mind, could not resist crossing the Platz, and
shouting into the great gateway of the palace, "Lobkowitzscher Esel"
(ass of a Lobkowitz).

Meanwhile, the French army, with Napoleon at its head, was advancing on
Vienna and almost at the time that the opera was ready for presentation,
took possession of the city. This was on November 13, 1805. The
imperial family, the members of the nobility and every one else who
could do so, had left the city on the approach of the French forces, but
this did not discourage Beethoven. The opera was ready and must be
presented. He could not have expected much of an audience as the very
people who were interested in the subject had left the city. It was
actually put on the stage on November 20, the audience consisting, it
appears, mainly of French officers. It is not to be supposed that such a
work would appeal to them, as there was no ballet, and the melodrama,
instead of containing good jokes and risque anecdotes, was simply the
tale of a wife's devotion. No doubt the intendant of the theatre, as
well as Beethoven and the whole company were anathematized freely. It
was continued for three nights and then withdrawn.

The work involved was enormous, both in the composition and in getting
it ready for the stage. The rewards during Beethoven's lifetime were
always slow. In its original form the opera was considered too long for
the patience of the average audience, and also in parts too abstruse,
which latter was probably its chief fault. The idea of revising it does
not seem to have occurred to Beethoven, even after it was withdrawn; it
required the utmost diplomacy on the part of his friends, Prince
Lichnowsky in particular, to bring this about.

Beethoven had taken extraordinary pains with it up to the time of its
representation. To make alterations now would be to acknowledge himself
in error. The opera, however, was the most ambitious work he had yet
attempted; to make it a success it was necessary that it be revised and
altered considerably. With this object in view, Beethoven was invited by
Prince Lichnowsky to meet some friends at his house to discuss the
opera. The singers, Roeke and Meyer, who appeared in the cast, were of
the party; also Stephen von Breuning and Sonnleithner. The score was
studied at the piano and freely criticised. When one of the singers
plainly stated that several pieces should be omitted entire and other
portions shortened, Beethoven's rage knew no bounds. The conflict lasted
well into the night, Beethoven at bay, with all his friends pitted
against him. He defended every attack on this child of his brain, the
latest product of his genius, and at first refused any compromise, but
better counsels finally prevailed, aided probably by the Princess
Lichnowsky, who so often assumed the part of peacemaker. Beethoven
consented to some important excisions, and an entire revision of the
opera. Stephen von Breuning, who was somewhat of a poet, and had
considerable literary ability, was commissioned to make the desired
changes in the libretto, cutting it down to two acts from three. The
conference lasted until one in the morning, when, the point being
gained, the Prince ordered supper to be brought in. Being Germans and
musicians, they finished the night in the utmost good humor, Beethoven
being the best natured of all, once his consent to the revision had been
gained.

He immediately set about writing a new overture for it, and that
imperishable work of genius, the Third Leonore overture appeared. Here
we have an epitome of the succeeding music of the opera, foreshadowing
in dramatic language, the grief and despair, and the final deliverance
and joy of the principal actors of the drama. Wagner says of this work,
"It is no longer an overture, but the mightiest of dramas in itself."
Here Beethoven could use his accustomed freedom once more. He was back
again in the familiar realm of instrumental music, and the storm and
stress of recent experiences no doubt supplied some of the material
which went into it. It is frequently used as a concert work.

The opera was produced the following spring in the revised form and with
the new overture. The wisdom of the revision was at once apparent, but a
quarrel between Beethoven and the intendant of the theatre led to its
final withdrawal after two representations. It did not see the light
again until 1814.

It was about this time that Beethoven first met Cherubini, whose operas
were favorites with the Vienna public. The Italian master made a stay of
several months' duration in Vienna, and attended a performance of
Fidelio.