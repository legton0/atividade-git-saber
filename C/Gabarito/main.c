#include <stdio.h>
#include "leitor.h"
#include "escritor.h"

int main(int argc, char **argv, char **envp)
{
	FILE *dados = fopen("data.dat", "r");
	FILE *copia = fopen("data_copy.dat", "w");

	if(!dados || !copia) return -1;

	while(feof(dados) == 0)
	{
		char ch = leia(dados);
		escreve(ch, copia);
	}

	return 0;
}